import React, { useEffect, useRef, useState } from 'react';
import useGoods from 'redux/data/hooks';
import { Item } from 'redux/data/data';
import fakeData from '../../assets/data/data.json';

const FETCH_INTERVAL = 15000;

function useInterval(callback: Function, delay: number) {
  const savedCallback = useRef<Function>();

  useEffect(() => {
    savedCallback.current = callback;
  });

  useEffect(() => {
    function tick() {
      if (savedCallback && savedCallback.current) savedCallback.current();
    }

    const id = setInterval(tick, delay);
    return () => clearInterval(id);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [delay]);
}

const Home: React.FC = () => {
  const {
    data: { products, cartTotal },
    updateData,
    addToCart,
    removeFromCart,
    changeQuantityInCart,
  } = useGoods();

  const [showCart, setShowCart] = useState(false);

  const fetchData = () => {
    try {
      // можно заменить на реальный запрос в будущем
      const { Success: success, Error: err, Value } = fakeData;

      if (!success) {
        // сервер вернул ошибку
        if (err) {
          // сервер вернул сообщение об ошибке
          throw Error(err);
        }
        throw Error('Server error');
      }

      updateData(Value.Goods);
    } catch (e) {
      console.log(e);
    }
  };

  useEffect(() => {
    fetchData();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useInterval(() => {
    fetchData();
  }, FETCH_INTERVAL);

  const onItemClick = (id: string, item: Item) => {
    if (item.T === 0) {
      alert('К сожалению данного товара больше нет в наличии');
      return;
    }
    // eslint-disable-next-line no-restricted-globals
    if (confirm(`Добавить ${item.N} в корзину?`)) {
      if (item.inCart) {
        // данный товар уже находиться в карзине, изменим кол-во
        changeQuantityInCart(id, item.itemsInCart + 1);
        return;
      }

      addToCart(id);
    }
  };

  const onDeleteClick = (id: string, name: string) => {
    // eslint-disable-next-line no-restricted-globals
    if (confirm(`Удалить ${name} из корзины?`)) {
      removeFromCart(id);
    }
  };

  const onChangeQuantityClick = (id: string, quantity: string) => {
    changeQuantityInCart(id, +quantity);
  };

  const toggleCart = () => setShowCart(!showCart);

  return (
    <div className="container">
      <div className="cart-container" style={{ height: showCart ? 'auto' : '0px' }}>
        <div className="cart">
          <div className="th">Наименование</div>
          <div className="th">Количество</div>
          <div className="th">Цена</div>
          <div className="th" />
          {Object.keys(products).map((catId) => {
            const { B } = products[catId];
            return Object.keys(B).map((itemId) => {
              const { N, C, T, inCart, itemsInCart } = B[itemId];
              if (!inCart) return null;
              return (
                <>
                  <div className="td">{N}</div>
                  <div className="td quantity">
                    <div className="input-container">
                      <input
                        type="number"
                        id={itemId}
                        name="input"
                        value={itemsInCart}
                        max={itemsInCart + T}
                        min={0}
                        onChange={(ev) => onChangeQuantityClick(itemId, ev.target.value)}
                      />
                      шт.
                    </div>
                    {T <= 3 && <div className="limited-quantity">Количество ограничено</div>}
                  </div>
                  <div className="td">{C.toFixed(2)} руб./шт.</div>
                  <div className="td">
                    <div
                      role="button"
                      tabIndex={0}
                      className="button"
                      onClick={() => onDeleteClick(itemId, B[itemId].N)}
                      onKeyDown={() => onDeleteClick(itemId, B[itemId].N)}
                    >
                      Удалить
                    </div>
                  </div>
                </>
              );
            });
          })}
          <div className="total">
            Общая стоимость: <span>{cartTotal.toFixed(2)} руб.</span>
          </div>
        </div>
      </div>
      <div
        className="cart-toggle button"
        onClick={toggleCart}
        onKeyDown={toggleCart}
        role="button"
        tabIndex={0}
      >
        {showCart
          ? 'Скрыть корзину'
          : `Показать корзину (Общая стоимость: ${cartTotal.toFixed(2)} руб.)`}
      </div>
      <div className="items-container">
        {Object.keys(products).map((catId) => {
          const { G, B } = products[catId];
          return (
            <div key={catId} className="category">
              <div className="header">{G}</div>
              {Object.keys(B).map((itemId) => {
                const { N, T, C, delta } = B[itemId];
                // если у товара нет цены, пропускаем
                if (!C) return null;
                let priceChange = '';
                if (delta > 0) {
                  priceChange = 'less';
                }
                if (delta < 0) {
                  priceChange = 'more';
                }
                return (
                  <div
                    key={itemId}
                    role="button"
                    tabIndex={0}
                    className="item button"
                    onClick={() => onItemClick(itemId, B[itemId])}
                    onKeyDown={() => onItemClick(itemId, B[itemId])}
                  >
                    <div className="item-name">
                      {N} ({T})
                    </div>
                    <div className={`item-price ${priceChange}`}>{C.toFixed(2)}</div>
                  </div>
                );
              })}
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default Home;
