import { Dispatch } from 'redux';
import names from '../../assets/data/names.json';

const initialState = {
  cartTotal: 0,
  products: JSON.parse(JSON.stringify(names)),
};

interface ResponseItem {
  B: boolean;
  C: number;
  CV: null;
  G: number;
  P: number;
  Pl: null;
  T: number;
}

export interface Response extends Array<ResponseItem> {
  [index: number]: ResponseItem;
}

export interface Item {
  N: string; // название товара
  T: number; // остаток
  C: number; // цена
  delta: number; // разница цены с предыдущей
  inCart: boolean; // есть ли этот товар в корзине
  itemsInCart: number; // кол-во в корзине
}

interface Items {
  [id: string]: Item;
}

interface Category {
  G: string; // название категории
  B: Items;
}

interface Products {
  [id: string]: Category;
}

export interface State {
  products: Products;
  cartTotal: number;
}

const SET_DATA = 'SET_DATA';

export const addCartItem = (id: string) => (
  dispatch: Dispatch,
  getState: () => { data: State }
): void => {
  const { products, cartTotal } = getState().data;
  const newProducts = JSON.parse(JSON.stringify(products));
  let found = false;
  let newCartTotal = cartTotal;

  Object.keys(products).forEach((catId) => {
    const item = newProducts[catId].B[id];
    if (item) {
      if (!item.inCart) {
        found = true;
        item.inCart = true;
        item.T -= 1;
        if (typeof item.itemsInCart === 'number') {
          item.itemsInCart += 1;
        } else {
          item.itemsInCart = 1;
        }
        newCartTotal += item.C;
      }
    }
  });

  // нет изменений
  if (!found) return;

  const data = {
    products: newProducts,
    cartTotal: newCartTotal,
  };

  dispatch({
    data,
    type: SET_DATA,
  });
};
export const changeCartItemQuantity = (id: string, qty: number) => (
  dispatch: Dispatch,
  getState: () => { data: State }
): void => {
  const { products, cartTotal } = getState().data;
  const newProducts = JSON.parse(JSON.stringify(products));
  let found = false;
  let newCartTotal = cartTotal;

  Object.keys(products).forEach((catId) => {
    const item = newProducts[catId].B[id];
    if (item) {
      if (item.inCart) {
        found = true;

        // сбросим все, перед тем, как поменять кол-во в корзине на новое
        item.T += item.itemsInCart;
        newCartTotal -= item.itemsInCart * item.C;
        item.itemsInCart = 0;

        let quantity = qty;
        // на всякий случай убедимся, что требуемое кол-во не превышает, то что мы имеем
        if (quantity > item.T) quantity = item.T;
        item.T -= quantity;
        item.itemsInCart = quantity;
        newCartTotal += item.itemsInCart * item.C;
      }
    }
  });

  // нет изменений
  if (!found) return;

  const data = {
    products: newProducts,
    cartTotal: newCartTotal,
  };

  dispatch({
    data,
    type: SET_DATA,
  });
};

export const removeCartItem = (id: string) => (
  dispatch: Dispatch,
  getState: () => { data: State }
): void => {
  const { products, cartTotal } = getState().data;
  const newProducts = JSON.parse(JSON.stringify(products));
  let found = false;
  let newCartTotal = cartTotal;

  Object.keys(products).forEach((catId) => {
    const item = newProducts[catId].B[id];
    if (item) {
      found = true;
      item.inCart = false;
      item.T = item.itemsInCart;
      newCartTotal -= item.itemsInCart * item.C;
      item.itemsInCart = 0;
    }
  });

  // нет изменений
  if (!found) return;

  const data = {
    products: newProducts,
    cartTotal: newCartTotal,
  };

  dispatch({
    data,
    type: SET_DATA,
  });
};

export const setDataFromResponse = (response: Response) => (
  dispatch: Dispatch,
  getState: () => { data: State }
): void => {
  const { products, cartTotal } = getState().data;
  const newProducts = JSON.parse(JSON.stringify(products));
  const exchangeRate = Math.random() * (80 - 20) + 20;
  let newCartTotal = cartTotal;

  // TODO: оповещать пользователя, о изменениях для товаров в корзине

  response.forEach(({ G, T, P, C }) => {
    const newPrice = C * exchangeRate;
    const item = newProducts[G].B[T];
    if (item.C !== undefined) item.delta = item.C - newPrice;

    if (item.inCart) {
      // данный товар есть в корзине, обновим корзину

      // пока уберем все его кол-во из корзины
      newCartTotal -= item.itemsInCart * item.C;

      // ...и внесем изменения в товар
      if (P === 0) {
        // товар закончился на складе
        item.inCart = false;
        item.itemsInCart = 0;
        item.T = P;
      } else if (P < item.itemsInCart) {
        // товара осталось меньше, чем в корзине
        item.itemsInCart = P;
        item.T = 0;
      } else {
        item.T = P - item.itemsInCart;
      }

      item.C = newPrice;
      // теперь можем опять добавить его, но уже с обновленной информацией
      newCartTotal += item.itemsInCart * item.C;
    } else {
      item.C = newPrice;
      item.T = P;
    }
  });

  const data = {
    products: newProducts,
    cartTotal: newCartTotal,
  };

  dispatch({
    data,
    type: SET_DATA,
  });
};

export default (state = initialState, action: { data: State; type: string }): State => {
  switch (action.type) {
    case SET_DATA:
      return action.data;
    default:
      return state;
  }
};
