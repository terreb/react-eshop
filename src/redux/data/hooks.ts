import { useDispatch, useSelector } from 'react-redux';
import {
  setDataFromResponse,
  addCartItem,
  removeCartItem,
  changeCartItemQuantity,
  State,
  Response,
} from 'redux/data/data';

export default (): {
  data: State;
  updateData: (data: Response) => void;
  addToCart: (id: string) => void;
  removeFromCart: (id: string) => void;
  changeQuantityInCart: (id: string, quantity: number) => void;
} => {
  const data = useSelector((state: { data: State }) => {
    return state.data;
  });
  const dispatch = useDispatch();

  const updateData = (response: Response): void => {
    dispatch(setDataFromResponse(response));
  };

  const addToCart = (id: string): void => {
    dispatch(addCartItem(id));
  };

  const removeFromCart = (id: string): void => {
    dispatch(removeCartItem(id));
  };

  const changeQuantityInCart = (id: string, quantity: number) => {
    dispatch(changeCartItemQuantity(id, quantity));
  };

  return { data, updateData, addToCart, removeFromCart, changeQuantityInCart };
};
