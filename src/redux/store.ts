import { applyMiddleware, combineReducers, createStore } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import logger from 'redux-logger';
import data from './data/data';

const rootReducer = combineReducers({ data });
const middleware: any = [thunk];
if (process.env.NODE_ENV === 'development') {
  middleware.push(logger);
}

export default createStore(rootReducer, composeWithDevTools(applyMiddleware(...middleware)));
