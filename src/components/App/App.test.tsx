import { render } from '@testing-library/react';
import React from 'react';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import App from './App';

const mockStore = configureStore();

test('sanity', () => {
  const { getByText } = render(
    <Provider store={mockStore({})}>
      <App />
    </Provider>
  );
  const text = getByText(/Совершенный код/i);

  expect(text).toBeInTheDocument();
});
